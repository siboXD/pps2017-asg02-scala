package lab05.solution

/** Consider the Parser example shown in previous lesson.
  * Analogously to NonEmpty, create a mixin NotTwoConsecutive,
  * which adds the idea that one cannot parse two consecutive
  * elements which are equal.
  * Use it (as a mixin) to build class NotTwoConsecutiveParser,
  * used in the testing code at the end.
  * Note we also test that the two mixins can work together!!
  */

abstract class Parser[T] {
  def parse(t: T): Boolean  // is the token accepted?
  def end(): Boolean        // is it ok to end here
  def parseAll(seq: Seq[T]): Boolean = (seq forall {parse(_)}) & end() // note &, not &&
}

case class BasicParser(chars: Set[Char]) extends Parser[Char] {
  override def parse(t: Char): Boolean = chars.contains(t)
  override def end(): Boolean = true
}

trait NonEmpty[T] extends Parser[T]{
  private[this] var empty = true
  abstract override def parse(t: T) = {empty = false; super.parse(t)} // who is super??
  abstract override def end() = !empty && {empty = true; super.end()}
}

class NonEmptyParser(chars: Set[Char]) extends BasicParser(chars) with NonEmpty[Char]

trait NotTwoConsecutive[T] extends Parser[T]{
  private[this] var previousElement: Option[T] = None

  abstract override def parse(t: T): Boolean = (previousElement.isEmpty || previousElement.get != t) && {
    previousElement = Some(t)
    super.parse(t)
  }
}

class NotTwoConsecutiveParser(chars: Set[Char]) extends BasicParser(chars) with NotTwoConsecutive[Char]


object TryParsers extends App {

  println(new BasicParser(Set('a', 'b', 'c')).parseAll("aabc".toList)) // true
  println(new BasicParser(Set('a', 'b', 'c')).parseAll("aabcdc".toList)) // false
  println(new BasicParser(Set('a', 'b', 'c')).parseAll("".toList)) // true

  // Note NonEmpty being "stacked" on to a concrete class
  // Bottom-up decorations: NonEmptyParser -> NonEmpty -> BasicParser -> Parser
  println(new NonEmptyParser(Set('0', '1')).parseAll("0101".toList)) // true
  println(new NonEmptyParser(Set('0', '1')).parseAll("0123".toList)) // false
  println(new NonEmptyParser(Set('0', '1')).parseAll(List())) // false

  println(new NotTwoConsecutiveParser(Set('X', 'Y', 'Z')).parseAll("XYZ".toList)) // true
  println(new NotTwoConsecutiveParser(Set('X', 'Y', 'Z')).parseAll("XYYZ".toList)) // false
  println(new NotTwoConsecutiveParser(Set('X', 'Y', 'Z')).parseAll("".toList)) // true

  // note we do not need a class name here, we use the structural type
  println(new BasicParser(Set('X', 'Y', 'Z')) with NotTwoConsecutive[Char] with NonEmpty[Char].parseAll("XYZ".toList)) // true
  println(new BasicParser(Set('X', 'Y', 'Z')) with NotTwoConsecutive[Char] with NonEmpty[Char].parseAll("XYYZ".toList)) // false
  println(new BasicParser(Set('X', 'Y', 'Z')) with NotTwoConsecutive[Char] with NonEmpty[Char].parseAll("".toList)) // false

}


