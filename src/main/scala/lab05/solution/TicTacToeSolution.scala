package lab05.solution

import scala.annotation.tailrec
import scala.language.postfixOps

object TicTacToeSolution extends App {
  sealed trait Player{
    def other: Player = this match {case X => O; case _ => X}
    override def toString: String = this match {case X => "X"; case _ => "O"}
  }
  case object X extends Player
  case object O extends Player

  case class Mark(x: Double, y: Double, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Double, y: Double): Option[Player] =
    board find (mark => mark.x == x && mark.y == y) map (_.player)

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    var generatedBoards: List[Board] = List()
    for (x <- 0 to 2; y <- 0 to 2) {
      if (find(board, x, y) isEmpty) {
        generatedBoards = generatedBoards :+ (Mark(x, y, player) :: board)
      }
    }
    generatedBoards
  }

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = {

    @tailrec
    def _computeAnyGame(alreadyComputedGames: Stream[Game],
                        nextMove: Int,
                        maxMoves: Int,
                        nextPlayer: Player): Stream[Game] =
      if (nextMove < maxMoves)
        _computeAnyGame(
          alreadyComputedGames flatMap (game => computePossibleSubsequentGames(game, nextPlayer.other)),
          nextMove + 1,
          maxMoves,
          nextPlayer.other
        )
      else alreadyComputedGames

    _computeAnyGame(Stream(List(List())), 0, moves, player)
  }

  def computePossibleSubsequentGames(game: Game, player: Player): Stream[Game] =
    placeAnyMark(game.head, player) map (_ :: game) toStream


  /* Instead of modifying above code i've reimplemented the computeAnyGame with end check to keep here the two versions at same time */

  def computeAnyGameWithEndCheck(player: Player, moves: Int): Stream[Game] = {

    def _computePossibleSubsequentGamesWithEndCheck(game: Game, player: Player): Stream[Game] = game.head match {
      case board if getWinner(board) isEmpty => computePossibleSubsequentGames(game, player)
      case _ => Stream(game)
    }

    @tailrec
    def _computeAnyGameWithEndCheck(alreadyComputedGames: Stream[Game],
                                    nextMove: Int,
                                    maxMoves: Int,
                                    nextPlayer: Player): Stream[Game] =
      if (nextMove < maxMoves)
        _computeAnyGameWithEndCheck(
          alreadyComputedGames flatMap (game => _computePossibleSubsequentGamesWithEndCheck(game, nextPlayer.other)),
          nextMove + 1,
          maxMoves,
          nextPlayer.other
        )
      else alreadyComputedGames

    _computeAnyGameWithEndCheck(Stream(List(List())), 0, moves, player)
  }

  def getWinner(board: Board): Option[Player] = {
    if (board.size < 3) None
    else {

      val winningMarks: ((_, List[_])) => Boolean = tuple => tuple._2.size == 3

      val playerVerticalMarks: Mark => (Double, Player) = mark => (mark.x, mark.player)
      val playerHorizontalMarks: Mark => (Double, Player) = mark => (mark.y, mark.player)
      val diagonalMarks: Mark => Boolean = mark => mark.x == mark.y
      val antiDiagonalMarks: Mark => Boolean = mark => mark.x + mark.y == 2

      val markToPlayer: PartialFunction[Mark, Player] = {
        case Mark(_, _, player) => player
      }

      val boardViews = Seq(
        board groupBy playerVerticalMarks,
        board groupBy playerHorizontalMarks,
        board filter diagonalMarks groupBy (_.player),
        board filter antiDiagonalMarks groupBy (_.player)
      )

      boardViews map (_ filter winningMarks map (_._2.head) collectFirst markToPlayer) collectFirst { case Some(player) => player }
    }
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...

  println("\ngetWinner test")
  private val game0 = List(List())
  printBoards(game0)
  println(getWinner(game0.head) + "\n") //None
  private val game1 = List(List(Mark(0, 0, X), Mark(0, 1, X), Mark(0, 2, X)))
  printBoards(game1)
  println(getWinner(game1.head) + "\n") // Some(X)
  private val game2 = List(List(Mark(0, 0, X), Mark(1, 1, X), Mark(0, 2, X)))
  printBoards(game2)
  println(getWinner(game2.head) + "\n") // None
  private val game3 = List(List(Mark(0, 0, X), Mark(1, 1, X), Mark(0, 2, X), Mark(2, 2, X)))
  printBoards(game3)
  println(getWinner(game3.head) + "\n") // Some(X)
  private val game4 = List(List(Mark(0, 0, X), Mark(1, 1, X), Mark(0, 2, X), Mark(2, 2, O)))
  printBoards(game4)
  println(getWinner(game4.head) + "\n") // None

  println("Compute any game: ")
  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  computeAnyGame(O, 4) foreach {g => printBoards(g); println()}
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
  println("Compute anyGameWithEndCheck: ")
  computeAnyGameWithEndCheck(O, 6) foreach { g => printBoards(g); println() }
}